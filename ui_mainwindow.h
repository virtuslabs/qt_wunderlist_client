/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_4;
    QLabel *userNameLabel;
    QSpacerItem *horizontalSpacer;
    QLabel *label_3;
    QLabel *userEmailLabel;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *taskListRefreshButton;
    QPushButton *taskListAddButton;
    QListWidget *taskListWidget;
    QGroupBox *tasksGroupBox;
    QVBoxLayout *verticalLayout_10;
    QPushButton *addTaskButton;
    QListWidget *tasksListWidget;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *subTaskGroupBox;
    QPushButton *subTaskAddButton;
    QGroupBox *groupBox_2;
    QPushButton *tasksAddButton_2;
    QDateEdit *taskDueDateEdit;
    QGroupBox *groupBox_3;
    QDateTimeEdit *taskReminderDateTimeEdit;
    QPushButton *tasksAddButton_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QVBoxLayout *verticalLayout_9;
    QLabel *label;
    QVBoxLayout *verticalLayout_3;
    QListWidget *taskNotesListWidget;
    QTextEdit *taskNoteTextEdit;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_2;
    QVBoxLayout *verticalLayout_4;
    QListWidget *commentsListWidget;
    QTextEdit *commentTextEdit;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuActions;
    QMenu *menuAccount;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1356, 460);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMaximumSize(QSize(16777215, 16777215));
        MainWindow->setAutoFillBackground(false);
        MainWindow->setStyleSheet(QStringLiteral(""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        centralWidget->setMinimumSize(QSize(0, 400));
        verticalLayout_5 = new QVBoxLayout(centralWidget);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_2->addWidget(label_4);

        userNameLabel = new QLabel(centralWidget);
        userNameLabel->setObjectName(QStringLiteral("userNameLabel"));
        userNameLabel->setMaximumSize(QSize(16777215, 50));

        horizontalLayout_2->addWidget(userNameLabel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMaximumSize(QSize(16777215, 50));

        horizontalLayout_2->addWidget(label_3);

        userEmailLabel = new QLabel(centralWidget);
        userEmailLabel->setObjectName(QStringLiteral("userEmailLabel"));
        userEmailLabel->setMaximumSize(QSize(16777215, 50));

        horizontalLayout_2->addWidget(userEmailLabel);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        groupBox_5 = new QGroupBox(centralWidget);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy1);
        groupBox_5->setMinimumSize(QSize(200, 0));
        groupBox_5->setMaximumSize(QSize(150, 16777215));
        verticalLayout_7 = new QVBoxLayout(groupBox_5);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        taskListRefreshButton = new QPushButton(groupBox_5);
        taskListRefreshButton->setObjectName(QStringLiteral("taskListRefreshButton"));
        taskListRefreshButton->setAutoFillBackground(false);
        taskListRefreshButton->setStyleSheet(QLatin1String("background-image: url(:/icons/icons/filerefresh16.png);\n"
"background-repeat: none;"));
        taskListRefreshButton->setFlat(true);

        horizontalLayout_4->addWidget(taskListRefreshButton);

        taskListAddButton = new QPushButton(groupBox_5);
        taskListAddButton->setObjectName(QStringLiteral("taskListAddButton"));
        taskListAddButton->setAutoFillBackground(false);
        taskListAddButton->setStyleSheet(QLatin1String("background-image: url(:/icons/icons/addfile16.png);\n"
"background-repeat: none;"));
        taskListAddButton->setFlat(true);

        horizontalLayout_4->addWidget(taskListAddButton);


        verticalLayout_7->addLayout(horizontalLayout_4);

        taskListWidget = new QListWidget(groupBox_5);
        taskListWidget->setObjectName(QStringLiteral("taskListWidget"));

        verticalLayout_7->addWidget(taskListWidget);


        horizontalLayout->addWidget(groupBox_5);

        tasksGroupBox = new QGroupBox(centralWidget);
        tasksGroupBox->setObjectName(QStringLiteral("tasksGroupBox"));
        sizePolicy.setHeightForWidth(tasksGroupBox->sizePolicy().hasHeightForWidth());
        tasksGroupBox->setSizePolicy(sizePolicy);
        tasksGroupBox->setMinimumSize(QSize(600, 0));
        verticalLayout_10 = new QVBoxLayout(tasksGroupBox);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        addTaskButton = new QPushButton(tasksGroupBox);
        addTaskButton->setObjectName(QStringLiteral("addTaskButton"));
        addTaskButton->setAutoFillBackground(false);
        addTaskButton->setStyleSheet(QLatin1String("background-image: url(:/icons/icons/addfile16.png);\n"
"background-repeat: none;"));
        addTaskButton->setFlat(true);

        verticalLayout_10->addWidget(addTaskButton, 0, Qt::AlignRight);

        tasksListWidget = new QListWidget(tasksGroupBox);
        tasksListWidget->setObjectName(QStringLiteral("tasksListWidget"));
        tasksListWidget->setProperty("isWrapping", QVariant(false));
        tasksListWidget->setViewMode(QListView::ListMode);
        tasksListWidget->setWordWrap(true);

        verticalLayout_10->addWidget(tasksListWidget);


        horizontalLayout->addWidget(tasksGroupBox);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        subTaskGroupBox = new QGroupBox(centralWidget);
        subTaskGroupBox->setObjectName(QStringLiteral("subTaskGroupBox"));
        sizePolicy.setHeightForWidth(subTaskGroupBox->sizePolicy().hasHeightForWidth());
        subTaskGroupBox->setSizePolicy(sizePolicy);
        subTaskGroupBox->setMinimumSize(QSize(250, 0));
        subTaskAddButton = new QPushButton(subTaskGroupBox);
        subTaskAddButton->setObjectName(QStringLiteral("subTaskAddButton"));
        subTaskAddButton->setGeometry(QRect(230, 0, 21, 20));
        subTaskAddButton->setAutoFillBackground(false);
        subTaskAddButton->setStyleSheet(QLatin1String("background-image: url(:/icons/icons/addfile16.png);\n"
"background-repeat: none;"));
        subTaskAddButton->setFlat(true);

        verticalLayout_2->addWidget(subTaskGroupBox);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        sizePolicy.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy);
        groupBox_2->setFlat(false);
        groupBox_2->setCheckable(false);
        tasksAddButton_2 = new QPushButton(groupBox_2);
        tasksAddButton_2->setObjectName(QStringLiteral("tasksAddButton_2"));
        tasksAddButton_2->setGeometry(QRect(170, 50, 21, 20));
        tasksAddButton_2->setAutoFillBackground(false);
        tasksAddButton_2->setStyleSheet(QLatin1String("background-image: url(:/icons/icons/filesave16.png);\n"
"background-repeat: none;"));
        tasksAddButton_2->setFlat(true);
        taskDueDateEdit = new QDateEdit(groupBox_2);
        taskDueDateEdit->setObjectName(QStringLiteral("taskDueDateEdit"));
        taskDueDateEdit->setGeometry(QRect(30, 50, 110, 23));

        verticalLayout_2->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        sizePolicy.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy);
        taskReminderDateTimeEdit = new QDateTimeEdit(groupBox_3);
        taskReminderDateTimeEdit->setObjectName(QStringLiteral("taskReminderDateTimeEdit"));
        taskReminderDateTimeEdit->setGeometry(QRect(20, 50, 145, 23));
        tasksAddButton_3 = new QPushButton(groupBox_3);
        tasksAddButton_3->setObjectName(QStringLiteral("tasksAddButton_3"));
        tasksAddButton_3->setGeometry(QRect(170, 50, 21, 20));
        tasksAddButton_3->setAutoFillBackground(false);
        tasksAddButton_3->setStyleSheet(QLatin1String("background-image: url(:/icons/icons/filesave16.png);\n"
"background-repeat: none;"));
        tasksAddButton_3->setFlat(true);

        verticalLayout_2->addWidget(groupBox_3);


        horizontalLayout->addLayout(verticalLayout_2);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy2);
        groupBox->setMinimumSize(QSize(0, 0));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        stackedWidget = new QStackedWidget(groupBox);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        QSizePolicy sizePolicy3(QSizePolicy::MinimumExpanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy3);
        stackedWidget->setMinimumSize(QSize(0, 325));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        QSizePolicy sizePolicy4(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(page->sizePolicy().hasHeightForWidth());
        page->setSizePolicy(sizePolicy4);
        page->setMinimumSize(QSize(0, 300));
        verticalLayout_9 = new QVBoxLayout(page);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        label = new QLabel(page);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_9->addWidget(label);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        taskNotesListWidget = new QListWidget(page);
        taskNotesListWidget->setObjectName(QStringLiteral("taskNotesListWidget"));

        verticalLayout_3->addWidget(taskNotesListWidget);

        taskNoteTextEdit = new QTextEdit(page);
        taskNoteTextEdit->setObjectName(QStringLiteral("taskNoteTextEdit"));

        verticalLayout_3->addWidget(taskNoteTextEdit);


        verticalLayout_9->addLayout(verticalLayout_3);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        sizePolicy4.setHeightForWidth(page_2->sizePolicy().hasHeightForWidth());
        page_2->setSizePolicy(sizePolicy4);
        page_2->setMinimumSize(QSize(0, 300));
        verticalLayout_8 = new QVBoxLayout(page_2);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        label_2 = new QLabel(page_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_8->addWidget(label_2);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        commentsListWidget = new QListWidget(page_2);
        commentsListWidget->setObjectName(QStringLiteral("commentsListWidget"));

        verticalLayout_4->addWidget(commentsListWidget);

        commentTextEdit = new QTextEdit(page_2);
        commentTextEdit->setObjectName(QStringLiteral("commentTextEdit"));

        verticalLayout_4->addWidget(commentTextEdit);


        verticalLayout_8->addLayout(verticalLayout_4);

        stackedWidget->addWidget(page_2);

        gridLayout->addWidget(stackedWidget, 0, 0, 1, 1);


        horizontalLayout->addWidget(groupBox);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_5->addLayout(verticalLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1356, 19));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuActions = new QMenu(menuBar);
        menuActions->setObjectName(QStringLiteral("menuActions"));
        menuAccount = new QMenu(menuBar);
        menuAccount->setObjectName(QStringLiteral("menuAccount"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuActions->menuAction());
        menuBar->addAction(menuAccount->menuAction());

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label_4->setText(QApplication::translate("MainWindow", "Current User:", 0));
        userNameLabel->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        label_3->setText(QApplication::translate("MainWindow", "Email:", 0));
        userEmailLabel->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "Task Lists", 0));
#ifndef QT_NO_TOOLTIP
        taskListRefreshButton->setToolTip(QApplication::translate("MainWindow", "Refresh Task List", 0));
#endif // QT_NO_TOOLTIP
        taskListRefreshButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        taskListAddButton->setToolTip(QApplication::translate("MainWindow", "Add New Task List", 0));
#endif // QT_NO_TOOLTIP
        taskListAddButton->setText(QString());
        tasksGroupBox->setTitle(QApplication::translate("MainWindow", "Tasks", 0));
#ifndef QT_NO_TOOLTIP
        addTaskButton->setToolTip(QApplication::translate("MainWindow", "Add New Task", 0));
#endif // QT_NO_TOOLTIP
        addTaskButton->setText(QString());
        subTaskGroupBox->setTitle(QApplication::translate("MainWindow", "SubTasks", 0));
#ifndef QT_NO_TOOLTIP
        subTaskAddButton->setToolTip(QApplication::translate("MainWindow", "Add New Subtask", 0));
#endif // QT_NO_TOOLTIP
        subTaskAddButton->setText(QString());
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Set Due Date", 0));
#ifndef QT_NO_TOOLTIP
        tasksAddButton_2->setToolTip(QApplication::translate("MainWindow", "Save Due Date", 0));
#endif // QT_NO_TOOLTIP
        tasksAddButton_2->setText(QString());
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Remind me...", 0));
#ifndef QT_NO_TOOLTIP
        tasksAddButton_3->setToolTip(QApplication::translate("MainWindow", "Save Reminder", 0));
#endif // QT_NO_TOOLTIP
        tasksAddButton_3->setText(QString());
        groupBox->setTitle(QString());
        label->setText(QApplication::translate("MainWindow", "Notes", 0));
        label_2->setText(QApplication::translate("MainWindow", "Comments", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
        menuActions->setTitle(QApplication::translate("MainWindow", "Actions", 0));
        menuAccount->setTitle(QApplication::translate("MainWindow", "Account", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
