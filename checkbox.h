#ifndef QCHECKBOX_H
#define QCHECKBOX_H

#include <QWidget>

class checkbox : public QWidget
{
    Q_OBJECT
public:
    explicit checkbox(QWidget *parent = 0);

private:
    void enterEvent(QEvent *event);

signals:

public slots:
};

#endif // QCHECKBOX_H
