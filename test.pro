#-------------------------------------------------
#
# Project created by QtCreator 2016-10-21T18:28:05
#
#-------------------------------------------------

QT       += core gui network websockets webview webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    echoserver.cpp \
    wunderlistclient.cpp \
    logindialog.cpp \
    oauth2.cpp \
    checkbox.cpp

HEADERS  += mainwindow.h \
    echoserver.h \
    wunderlistclient.h \
    logindialog.h \
    oauth2.h \
    checkbox.h

FORMS    += mainwindow.ui \
    logindialog.ui

DISTFILES += \
    icons/list.png \
    stylesheets/app.css

#icons.path=$${DESTDIR}/icons
#icons.files+=$$files(icons/*)
#stylesheets.path=$${DESTDIR}/stylesheets
#stylesheets.files+=$$files(stylesheets/*)

#INSTALLS+= icons stylesheets

#unix {
#    for(FILE, DISTFILES){
#        QMAKE_POST_LINK += $$quote(cp $${PWD}/$${FILE} $${DESTDIR}$$escape_expand(\\n\\t))
#}
#}

RESOURCES += \
    resources.qrc
