#include "oauth2.h"
#include <QDebug>
#include <QApplication>
#include "logindialog.h"
#include <QMessageBox>
#include <QSettings>

OAuth2::OAuth2(QWidget* parent)
{
    m_strEndPoint = "https://accounts.google.com/o/oauth2/auth";
    m_strScope = "https://www.googleapis.com/auth/books";
    m_strRedirectURI = "http://localhost/oauthcallback";
    m_strResponseType = "token";

    m_pLoginDialog = new LoginDialog(parent);
    m_pParent = parent;
    connect(m_pLoginDialog, SIGNAL(accessTokenObtained()), this, SLOT(accessTokenObtained()));
}

void OAuth2::setScope(const QString& scope)
{
    m_strScope = scope;
}

void OAuth2::setClientID(const QString& clientID)
{
    m_strClientID = clientID;
}

void OAuth2::setClientSecret(const QString& clientSecret)
{
    m_strClientSecret = clientSecret;
}

void OAuth2::setRedirectURI(const QString& redirectURI)
{
    m_strRedirectURI = redirectURI;
}

void OAuth2::authorize()
{
//    this->m_pLoginDialog->ui->webView.load(this->loginUrl());
    this->m_pLoginDialog->setLoginUrl(this->loginUrl());
    this->m_pLoginDialog->show();
}

QString OAuth2::loginUrl()
{
//    QString str = QString("%1?client_id=%2&redirect_uri=%3&response_type=%4&scope=%5").arg(m_strEndPoint).arg(m_strClientID).
//            arg(m_strRedirectURI).arg(m_strResponseType).arg(m_strScope);
    QString str = QString("https://www.wunderlist.com/oauth/authorize?client_id=%1&redirect_uri=%2&state=RANDOM")
            .arg(m_strClientID)
            .arg(m_strRedirectURI);
    qDebug() << "Login URL" << str;
    return str;
}

QString OAuth2::accessToken()
{
    return m_strAccessToken;
}

bool OAuth2::isAuthorized()
{
    return m_strAccessToken != "";
}

void OAuth2::accessTokenObtained()
{
    m_strAccessToken = m_pLoginDialog->accessToken();
//    *settings->setValue("auth_token", m_strAccessToken);
    emit loginDone(m_strAccessToken);

}


