#ifndef ECHOSERVER_H
#define ECHOSERVER_H

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QWidget>
#include <QtCore/QByteArray>
#include "mainwindow.h"

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

class EchoServer: public QObject
{
    Q_OBJECT
public:
    explicit EchoServer(quint16 port, QObject *parent = Q_NULLPTR);
    virtual ~EchoServer();
private:
    QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket *> m_clients;

Q_SIGNALS:
    void textMessageReceived(QString);

private Q_SLOTS:
    void onNewConnection();
    void processMessage(QString message);
    void socketDisconnected();
};

#endif // ECHOSERVER_H
