#include "oauth2.h"
#include "wunderlistclient.h"
#include <string>
#include <QDebug>
#include <QJsonObject>
#include <QByteArray>
#include <QUrlQuery>
#include <QListWidgetItem>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

WunderlistClient::WunderlistClient(QObject *parent, QSettings *settings) : QObject(parent)
{
    m_strApiUrl = "https://a.wunderlist.com/api/v1";
    m_strListsUrl = "https://a.wunderlist.com/api/v1/lists";
    m_strAccessTokenUrl = "https://www.wunderlist.com/oauth/access_token";
    m_strListTasksUrl = "https://a.wunderlist.com/api/v1/tasks";
    m_strUserUrl = "https://a.wunderlist.com/api/v1/user";
    m_strSubTasksUrl = "https://a.wunderlist.com/api/v1/subtasks";
    m_strReminderUrl= "https://a.wunderlist.com/api/v1/reminders";
    m_strClientId = "a0499a2e05de4be0ac56";
    m_strClientSecret = "3f3292cc3a61309d9cd1928357cd60da26eb93db5dc7ab8223504c04362a";
    m_networkManager = new QNetworkAccessManager(this);
    m_settingsFile = settings;
    m_strAuthToken = m_settingsFile->value("auth_token").toString();
    m_strAccessToken = m_settingsFile->value("access_token").toString();
    qDebug() << "token from settings: " << m_strAccessToken;

    if(m_strAccessToken == "")
    {
        OAuth2 *auth = new OAuth2();
        auth->setClientID(m_strClientId);
        auth->setClientSecret(m_strClientSecret);
        connect(auth, &OAuth2::loginDone, this, &WunderlistClient::getAccessToken);
        auth->authorize();
    }
    this->getUser();
    this->getTaskLists();

}

QNetworkReply* WunderlistClient::getAccessToken(QString authToken)
{

    this-> m_strAuthToken = authToken;
    this->m_settingsFile->setValue("auth_token", authToken);
    QNetworkRequest	request;
    QJsonObject data;
    QJsonDocument jDoc;
    request.setUrl(QUrl(QString("%1").arg(m_strAccessTokenUrl)));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    data["client_id"] = this->m_strClientId;
    data["client_secret"] = this->m_strClientSecret;
    data["code"] = m_strAuthToken;
    jDoc.setObject(data);
    qDebug() << "data: " << jDoc;
    QByteArray data_bytes = jDoc.toJson();
    accessTokenReply = m_networkManager->post(request, jDoc.toJson());
    connect(accessTokenReply, &QNetworkReply::finished, this, &WunderlistClient::setAccessToken);

    return accessTokenReply;

}

void WunderlistClient::setAccessToken()
{
    QJsonDocument replyData	= QJsonDocument::fromJson(accessTokenReply->readAll());
    QString accessToken = replyData.object().value("access_token").toString();
    qDebug() << "accessToken: " << replyData;
    this->m_settingsFile->setValue("access_token", accessToken);
    this->m_strAccessToken = accessToken;
    accessTokenReply->deleteLater();
    this->getUser();

//    this->getTaskLists();
}

QNetworkReply* WunderlistClient::getTaskLists()
{

    QNetworkRequest	request;
    request.setUrl(QUrl(QString("%1").arg(m_strListsUrl)));
    request.setRawHeader("X-Client-ID", this->m_strClientId.toUtf8());
    request.setRawHeader("X-Access-Token", this->m_strAccessToken.toUtf8());
//    QNetworkReply *reply;
    taskListsReply = m_networkManager->get(request);
    connect(taskListsReply, &QNetworkReply::finished, this, &WunderlistClient::setTaskLists);

    qDebug() << "got tasks...";


    return taskListsReply;
}

void WunderlistClient::setTaskLists()
{

    QJsonDocument replyData	= QJsonDocument::fromJson(taskListsReply->readAll());
//    qDebug() << taskListsReply->errorString();
//    qDebug() << replyData;
    QJsonObject obj = replyData.object();
//    this->m_settingsFile->setValue("access_token", accessToken);

    QJsonArray array = replyData.array();
    this->taskLists = array;
    foreach (const QJsonValue &value, array) {
        QJsonObject obj = value.toObject();
        qDebug() << "taskList: " << obj;
    }
    emit gotTaskLists(this->taskLists);
    taskListsReply->deleteLater();
}

QNetworkReply* WunderlistClient::getTaskListTasks(QListWidgetItem *item)
{
    QString title = item->text();
//    qDebug() << "title..." << title;
    double taskListId;
    foreach (const QJsonValue &value, this->taskLists) {
        QJsonObject obj = value.toObject();
        qDebug() << "objtitle" << obj["title"].toString();
        if(obj["title"].toString() == title)
        {
            taskListId = obj["id"].toDouble();
//            qDebug() << "tasklistid" << obj["id"].toDouble();
        }
    }
    QNetworkRequest	request;
    QUrlQuery query;
    query.addQueryItem("list_id", QString::number(taskListId, 'f', 0));
    QUrl url(QString("%1").arg(m_strListTasksUrl));
    url.setQuery(query);
//    qDebug() << url;
    request.setUrl(url);
    request.setRawHeader("X-Client-ID", this->m_strClientId.toUtf8());
    request.setRawHeader("X-Access-Token", this->m_strAccessToken.toUtf8());
    taskListsTasksReply = m_networkManager->get(request);
    connect(taskListsTasksReply, &QNetworkReply::finished, this, &WunderlistClient::setTaskListTasks);

    qDebug() << "got tasks...";


    return taskListsReply;
}

void WunderlistClient::setTaskListTasks()
{
    QJsonDocument replyData	= QJsonDocument::fromJson(taskListsTasksReply->readAll());
//    QJsonObject obj = replyData.object();
    this->taskListTasks = replyData.array();
//    qDebug() << this->taskListTasks;
    emit gotTaskListTasks(this->taskListTasks);
    taskListsTasksReply->deleteLater();
}

QDate WunderlistClient::getTaskDueDate(QListWidgetItem *item)
{
    foreach (const QJsonValue &value, this->taskListTasks) {
        QJsonObject obj = value.toObject();
        if(obj.value("title").toString() == item->text())
        {
            QDate *date;
            qDebug() << "Date" << obj.value("due_date").toString();
            return date->fromString(obj.value("due_date").toString(),"yyyy-MM-dd");
        }
    }
}

QNetworkReply* WunderlistClient::getUser()
{

    QNetworkRequest	request;
    request.setUrl(QUrl(QString("%1").arg(m_strUserUrl)));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setRawHeader("X-Client-ID", this->m_strClientId.toUtf8());
    request.setRawHeader("X-Access-Token", this->m_strAccessToken.toUtf8());
    userReply = m_networkManager->get(request);
    connect(userReply, &QNetworkReply::finished, this, &WunderlistClient::setUser);

    return userReply;

}

void WunderlistClient::setUser()
{
    QJsonDocument replyData	= QJsonDocument::fromJson(userReply->readAll());
    this->m_strUser = replyData.object().value("user").toString();
    this->m_strEmail = replyData.object().value("email").toString();
//    qDebug() << "accessToken: " << replyData;
    //TODO move this to mainwindow...keep client generic
//    this->m_settingsFile->setValue("user", user);
    userReply->deleteLater();
    emit gotUser(replyData);
//    this->getTaskLists();
}

QNetworkReply* WunderlistClient::getTaskReminder(QListWidgetItem *item)
{
    QJsonObject selectedItem;
    foreach (const QJsonValue &value, this->taskListTasks) {
        QJsonObject obj = value.toObject();
        if(obj.value("title").toString() == item->text())
        {
            selectedItem = obj;
        }
    }

    QNetworkRequest	request;
    QUrlQuery query;
    query.addQueryItem("task_id", QString::number(selectedItem.value("id").toDouble(), 'f', 0));
    QUrl url(QString("%1").arg(m_strReminderUrl));
    url.setQuery(query);
    request.setUrl(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setRawHeader("X-Client-ID", this->m_strClientId.toUtf8());
    request.setRawHeader("X-Access-Token", this->m_strAccessToken.toUtf8());
    taskReminderReply = m_networkManager->get(request);
    connect(taskReminderReply, &QNetworkReply::finished, this, &WunderlistClient::setReminder);

    return taskReminderReply;

}

void WunderlistClient::setReminder()
{
    QJsonDocument replyData	= QJsonDocument::fromJson(taskReminderReply->readAll());
    qDebug() << replyData;
    QJsonObject reminder = replyData.array().first().toObject();
    QDateTime reminderDate = QDateTime::fromString(reminder.value("date").toString(), "yyyy-MM-ddTHH:mm:ss.zzzZ");
    qDebug() << reminder.value("date").toString();
    reminderDate.setTimeSpec(Qt::UTC);
    qDebug() << reminderDate.isValid();
    this->taskReminderDate = reminderDate;

    emit gotReminders(this->taskReminderDate);
}
