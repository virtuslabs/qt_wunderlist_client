#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "wunderlistclient.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QWidgetItem>
#include <QSize>
#include <QFile>
#include <QCheckBox>
#include <QtWebSockets/QWebSocketServer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QFile File("stylesheets/app.css");
    File.open(QFile::ReadOnly);
    this->setStyleSheet(File.readAll());
    QString m_strCompanyName = "Virtus Labs";
    QString m_strAppName = "Cross-Platform Wunderlist Client";
    QString m_strSettingsFile = QApplication::applicationDirPath()+"/settings.ini";
    QSettings *settings = new QSettings(m_strSettingsFile,QSettings::IniFormat);
//    listWidget = new QListWidget(this);
//    tasksWidget = new QListWidget(this);
//    tasksWidget->resize(100, 200);
//    tasksWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
//    getListsButton = new QPushButton();
//    getListsButton->setText("Get Task Lists");
//    getListsButton->setStyleSheet("QPushButton{background-color: grey}");
//    this->getListsButton->setFlat(true);
//    this->getListsButton->setBaseSize(QSize(300, 50));
//    getListsButton->setIcon(QIcon("icons/list.png"));
//    getListsButton->setIconSize(QSize(32, 32));
    this->client = new WunderlistClient(this, settings);
//    QApplication::alert(this->ui->listsWidget, 2);
    connect(this->ui->taskListRefreshButton, &QPushButton::clicked, this->client, &WunderlistClient::getTaskLists);
    connect(client, &WunderlistClient::gotTaskLists, this, &MainWindow::updateListWidget);
    connect(this->client, &WunderlistClient::gotUser, this, &MainWindow::setUser);
    connect(this->ui->taskListWidget, &QListWidget::itemClicked, this->client, &WunderlistClient::getTaskListTasks);
    connect(this->ui->tasksListWidget, &QListWidget::itemClicked, this, &MainWindow::taskClicked);
    connect(this->client, &WunderlistClient::gotTaskListTasks, this, &MainWindow::updateListTasksWidget);
    connect(this->client, &WunderlistClient::gotReminders, this, &MainWindow::setReminders);

    this->setWindowTitle("Wunderlist Client");
//    this->ui->lineEdit1->setText("Initial Message");
    this->resize(1200, 1000);
//    this->ui->gridLayout->addWidget(listsWidget);
//    this->ui->gridLayout->addWidget(getListsButton);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateListWidget(QJsonArray lists)
{
    this->ui->taskListWidget->clear();
//    QJsonArray array = this->client->getLists();
//    qDebug() << "Returned Lists: " << lists;
    foreach (const QJsonValue &value, lists) {
        QJsonObject obj = value.toObject();
//        qDebug() << "taskList: " << obj;
        this->ui->taskListWidget->addItem(obj["title"].toString());
    }
//    QApplication::alert(this->listWidget, 2);
}

void MainWindow::updateListTasksWidget(QJsonArray tasks)
{
    qDebug() << "stopped?: ";
//    while (QLayoutItem* item = this->ui->tasksScrollAreaWidgetContents->layout()->takeAt(0))
//    {

//        if (QWidget* widget = item->widget())
//        {
//            widget->deleteLater();
//        }
//        delete item;
//    }
    this->ui->tasksListWidget->clear();
//    QJsonArray array = this->client->getListTasks();

//    QWidget *checkBoxes = new QWidget(this->ui->scrollArea_2);

    QListWidgetItem *items [tasks.count()];

    for (int i = 0; i < tasks.count(); i++) {

        const QJsonValue &value = tasks[i];
        QJsonObject obj = value.toObject();
//        pushButton[i] = new QLabel();
//        checkBox[i]->setGeometry(30,30,331,18);
//        checkBox->setMinimumSize(50, 50);
//        this->ui->scrollAreaWidgetContents_2->resize(this->ui->scrollArea_2->size().width(), this->ui->scrollArea_2->size().height());
//        this->ui->scrollAreaWidgetContents_2->adjustSize();
//        QDateTime dueDate;
//        dueDate.fromString(obj["due_date"].toString());
//        this->ui->scrollArea_2->adjustSize();

//        this->ui->tasksScrollAreaWidgetContents->layout()->addWidget(pushButton[i]);
        items[i] = new QListWidgetItem();
        items[i]->setText(obj["title"].toString());
        items[i]->setIcon(QIcon(":/icons/WLCheckbox-detailview-down@2x.png"));
        QDate now;
        QDate dueDate = this->client->getTaskDueDate(items[i]);
        if ( dueDate< QDate::currentDate() && dueDate.toString() != "")
        {
            items[i]->setBackgroundColor("red");
        }
        this->ui->tasksListWidget->addItem(items[i]);
//        pushButton[i]->setCheckable(true);
//        pushButton[i]->setFlat(true);
//        pushButton[i]->setWordWrap(true);
//        pushButton[i]->setText("<a href=\"whatever\">"+obj["title"].toString()+"</a>");

//        this->ui->taskDueDateTimeEdit->setDateTime(dueDate);
    }

//    this->ui->scrollAreaWidgetContents_2->resize(this->ui->scrollArea_2->size().width(), this->ui->scrollArea_2->size().height());
//      this->ui->tasksGroupBox->resize(this->ui->tasksScrollArea->size().width(), this->ui->tasksScrollArea->size().height());
//      this->ui->tasksGroupBox->adjustSize();
//    this->ui->scrollAreaWidgetContents_2->layout()->addWidget(checkBox);
//    this->ui->tasksScrollAreaWidgetContents->adjustSize();
}

void MainWindow::setLineEditText(QString text)
{
    qDebug() << text;
//    this->ui->lineEdit1->setText(text);
}

void MainWindow::setUser(QJsonDocument user)
{
    this->ui->userNameLabel->setText(user.object().value("name").toString());
    this->ui->userEmailLabel->setText(user.object().value("email").toString());
}

void MainWindow::taskClicked(QListWidgetItem *item)
{
    qDebug() << this->client->getTaskDueDate(item);
    this->ui->taskDueDateEdit->setDate(this->client->getTaskDueDate(item));
    this->client->getTaskReminder(item);
//    QListIterator<QObject *> i(this->ui->tasksScrollAreaWidgetContents->layout()->children());
//    while (i.hasNext())
//    {
//        QPushButton *b = qobject_cast<QPushButton*>( i.next() );
//        qDebug() << b->text();
//        if (b->isChecked()) {
//            b->setAutoExclusive(false);

//            b->setChecked(false);
//            b->setAutoExclusive(true);
//        }
//    }
//    QPushButton *senderButton = qobject_cast<QPushButton*>(sender());
//    for (auto button : this->ui->tasksScrollAreaWidgetContents->findChildren<QPushButton*>())
//    {
//        if (button->text() != senderButton->text() && button->isChecked())
//        {
//            button->setChecked(!button->isChecked());
//        }
//    }

//    QListWidgetItem *senderItem = qqobject_cast<QListWidgetItem*>(sender());
//    item->setTextColor(QColor("green"));
//    for (auto button : this->ui->tasksScrollAreaWidgetContents->findChildren<QLabel*>())
//    {
//        if (button->text() != senderLabel->text())
//        {
//            button->setEnabled(!button->isEnabled());
//        }
//    }
}
void MainWindow::setReminders(QDateTime reminderDate){
    this->ui->taskReminderDateTimeEdit->setDateTime(reminderDate);
}
