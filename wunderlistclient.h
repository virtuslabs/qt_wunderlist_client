#ifndef WUNDERLISTCLIENT_H
#define WUNDERLISTCLIENT_H

#include <QObject>
#include <QSettings>
#include <QJsonArray>
#include <QJsonDocument>
#include <QListWidgetItem>
#include <QtNetwork/QNetworkAccessManager>

class WunderlistClient : public QObject
{
    Q_OBJECT
public:
    explicit WunderlistClient(QObject *parent = 0, QSettings *settings = NULL);

    QNetworkReply* getTaskLists();
    QNetworkReply* getTaskListTasks(QListWidgetItem *item);
    QNetworkReply* getTaskReminder(QListWidgetItem *item);
    QDate getTaskDueDate(QListWidgetItem *item);

private:
    QNetworkAccessManager *m_networkManager;
    QNetworkReply* getAccessToken(QString accessToken);
    QNetworkReply* getUser();

    void setUser();
    void setAccessToken();
    void setReminder();
    QString m_strAccessToken;
    QString m_strAuthToken;
    QString m_strApiUrl;
    QString m_strListsUrl;
    QString m_strListTasksUrl;
    QString m_strAccessTokenUrl;
    QString m_strUserUrl;
    QString m_strSubTasksUrl;
    QString m_strReminderUrl;
    QString m_strClientId;
    QString m_strClientSecret;
    QString m_strUser;
    QString m_strEmail;
    QSettings *m_settingsFile;
    QJsonArray taskLists;
    QJsonArray taskListTasks;
    QDateTime taskReminderDate;
    QNetworkReply *accessTokenReply;
    QNetworkReply *taskListsReply;
    QNetworkReply *taskListsTasksReply;
    QNetworkReply *userReply;
    QNetworkReply *taskReminderReply;

    void setTaskLists();
    void setTaskListTasks();

signals:
    void gotTaskLists(QJsonArray taskLists);
    void gotTaskListTasks(QJsonArray taskListTasks);
    void gotUser(QJsonDocument user);
    void gotReminders(QDateTime reminderDate);
public slots:

};

#endif // WUNDERLISTCLIENT_H
