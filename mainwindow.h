#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QNetworkReply>
#include <wunderlistclient.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setClient(WunderlistClient *client);

private:
    Ui::MainWindow *ui;
//    QListWidget *listWidget;
    QListWidget *tasksWidget;
//    QPushButton *getListsButton;
    WunderlistClient *client;
    void updateListWidget(QJsonArray lists);
//    void getListTasks(QListWidgetItem item);
//    void setListTasks(QNetworkReply *reply);
    void updateListTasksWidget(QJsonArray tasks);
    void setUser(QJsonDocument user);
    void taskClicked(QListWidgetItem *item);
    void setReminders(QDateTime reminderDate);
signals:

public slots:
    void setLineEditText(QString);
};

#endif // MAINWINDOW_H
