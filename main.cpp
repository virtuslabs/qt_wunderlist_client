#include "mainwindow.h"
#include "echoserver.h"
#include "wunderlistclient.h"
#include <QApplication>
#include <QSettings>
#include <QFile>
#include "QWebSocket"
#include <QtCore/QObject>
#include <QtWebEngineWidgets/QWebEngineView>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;
//    QFile stylesheet("stylesheets/app.css");
//    QTextStream textStream(&stylesheet);
//    QString styleSheet = textStream.readAll();
//    stylesheet.close();
//    w.setStyleSheet(styleSheet);
    EchoServer *server = new EchoServer(8000, NULL);
    QObject::connect(server, &EchoServer::textMessageReceived, &w, &MainWindow::setLineEditText);
    w.show();

    return a.exec();
}
