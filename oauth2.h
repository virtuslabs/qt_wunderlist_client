#ifndef OAUTH2_H
#define OAUTH2_H
#include <QSettings>
#include <QString>
#include <QObject>

class LoginDialog;

class OAuth2 : public QObject
{
    Q_OBJECT

public:
    OAuth2(QWidget* parent = 0);
    QString accessToken();
    bool isAuthorized();

    //Functions to set application's details.
    void setScope(const QString& scope);
    void setClientID(const QString& clientID);
    void setRedirectURI(const QString& redirectURI);
    void setClientSecret(const QString& clientSecret);
    void authorize();

    QString loginUrl();

signals:
    //Signal that is emitted when login is ended OK.
    void loginDone(QString m_strAccessToken);

private slots:
    void accessTokenObtained();

private:
    QString m_strAccessToken;

    QString m_strEndPoint;
    QString m_strScope;
    QString m_strClientID;
    QString m_strClientSecret;
    QString m_strRedirectURI;
    QString m_strResponseType;

    LoginDialog* m_pLoginDialog;
    QWidget* m_pParent;
};

#endif // OAUTH2_H
