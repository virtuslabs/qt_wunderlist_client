#include "echoserver.h"
#include "mainwindow.h"
#include "QWidget"
#include "QtWebSockets/QWebSocket"
#include "QtWebSockets/QWebSocketServer"
//#include "QtNetwork/QHostAddress"
#include <QtCore/QDebug>

QT_USE_NAMESPACE

EchoServer::EchoServer(quint16 port, QObject *parent):
    QObject(parent),
    m_pWebSocketServer(Q_NULLPTR),
    m_clients()
{
    m_pWebSocketServer = new QWebSocketServer(QStringLiteral("server"),
                                            QWebSocketServer::NonSecureMode, this);
    if (m_pWebSocketServer->listen(QHostAddress::Any, port)){

        qDebug() << "Echoserver listening on port" << port;
        connect(m_pWebSocketServer, &QWebSocketServer::newConnection,
                this, &EchoServer::onNewConnection);
        connect(m_pWebSocketServer, &QWebSocketServer:: closed, this, &EchoServer::socketDisconnected);
    }
}

    EchoServer::~EchoServer()
    {
        m_pWebSocketServer->close();
        qDeleteAll(m_clients.begin(), m_clients.end());
    }

    void EchoServer::onNewConnection()
    {
        QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();
        connect(pSocket, &QWebSocket::textMessageReceived, this, &EchoServer::processMessage);
        connect(pSocket, &QWebSocket::disconnected, this, &EchoServer::socketDisconnected);
        m_clients << pSocket;
        qDebug() << m_clients.count();
    }

    void EchoServer::socketDisconnected()
    {
        QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
        if(pClient)
        {
            m_clients.removeAll(pClient);
            pClient->deleteLater();
        }
    }

    void EchoServer::processMessage(QString message)
    {
        QWebSocket *pSender = qobject_cast<QWebSocket *>(sender());
        Q_FOREACH (QWebSocket *pClient, m_clients)
        {
            if(pClient = pSender)
            {
                pClient->sendTextMessage(message);
                emit textMessageReceived(message);
//                this->parent()->ui->lineEdit1.setText(message);
            }
        }
    }

